from django.db import models


class News(models.Model):
    title = models.TextField()
    topics = models.ManyToManyField('topics.Topic', related_name='news_topic')
    meta = models.TextField()  # untuk isi berita
    image_url = models.TextField()  # link gambar
    author = models.CharField(max_length=50)  # pembuat berita
    status = models.CharField(max_length=20, default="")  # status

    # get topics many_to_many fields
    def get_topics(self):
        return ",".join([str(p) for p in self.topics.all()])

    def __str__(self):
        return self.topics