from rest_framework import serializers
from news import models
from topics import serializers as topic_serializers


# news serializer models
class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.News
        fields = ('title', 'get_topics', 'meta', 'image_url', 'author', 'status')
