from rest_framework import viewsets
from news import models as news_models
from news import serializers as news_serializers
from topics import models as topic_models
from topics import serializers as topic_serializers


class NewsView(viewsets.ModelViewSet):
    queryset = news_models.News.objects.all()
    serializer_class = news_serializers.NewsSerializer


class TopicViews(viewsets.ModelViewSet):
    queryset = topic_models.Topic.objects.all()
    serializer_class = topic_serializers.TopicSerializer

