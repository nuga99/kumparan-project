from django.contrib import admin
from news.models import News


class NewsAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'get_topics', 'meta', 'image_url', 'author', 'status')
    list_display_links = ('title',)
    search_fields = ('title', 'topics',)


admin.site.register(News, NewsAdmin)
