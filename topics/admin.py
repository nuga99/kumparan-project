from django.contrib import admin
from topics.models import Topic


class TopicAdmin(admin.ModelAdmin):
    list_display = ('id', 'topic', 'created_at')
    list_display_links = ('topic',)
    search_fields = ('topic',)


admin.site.register(Topic, TopicAdmin)
