from django.db import models


# Create your models here.
class Topic(models.Model):
    topic = models.CharField(max_length=30)
    news = models.ManyToManyField('news.News', related_name='news_title')
    created_at = models.DateTimeField(auto_now=True)

    def get_news(self):
        return ",".join([str(p) for p in self.news.all()])

    def __str__(self):
        return str(self.topic)
