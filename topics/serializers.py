from rest_framework import serializers
from topics import models
from news import serializers as news_serializers


# topic serializer models
class TopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Topic
        fields = ('id', 'topic', 'news', 'created_at', )
