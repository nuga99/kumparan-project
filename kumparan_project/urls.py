"""kumparan_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from news import api_views
from django.views.generic.base import RedirectView
from django.shortcuts import redirect

# router drf (django rest framework)
router_drf = routers.DefaultRouter()
router_drf.register(r'/list-news', api_views.NewsView)
router_drf.register(r'/list-topics', api_views.TopicViews)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/api/v1')),
    path('api/v1', include(router_drf.urls), name='api'),
    path('api-auth/', include('rest_framework.urls')),
]
